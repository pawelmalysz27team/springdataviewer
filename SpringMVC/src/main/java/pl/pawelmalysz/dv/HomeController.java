package pl.pawelmalysz.dv;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class HomeController {

	@RequestMapping(value = "/")
	public String home(Model model) {
		
		String s =ShowMe();
		
		model.addAttribute("dane", s);
		
		return "home";
	}
	
	public String ShowMe()
	{
		String out = "<table>";
		
		Connection c = null;
        int liczbaKolumn =0;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test","postgres", "post");
         Statement st = c.createStatement();
         ResultSet rs = st.executeQuery("select * from serie");
         ResultSetMetaData rsmd = rs.getMetaData();
         liczbaKolumn = rsmd.getColumnCount();
         
         while(rs.next())
         {
        	 out += "<tr>";
            for(int i=1;i<=liczbaKolumn;i++)
            {
                
                out += "<td>"+rs.getString(i)+"</td>";
            }
            out += "</tr>";
         }
         out += "</table>";
      } catch (Exception e) {
         e.printStackTrace();
         System.err.println(e.getClass().getName()+": "+e.getMessage());
         System.exit(0);
      }
      return out;
	}
	
}
