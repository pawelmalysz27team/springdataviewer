package pl.pawelmalysz.dv;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



@Configuration
@EnableWebMvc
@ComponentScan("pl.pawelmalysz.dv")
public class WebConfig implements WebMvcConfigurer{
	
	
	public void configureViewResolvers(ViewResolverRegistry registry)
	{
		registry.jsp();
	}

	
}
